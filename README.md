# `elasticsearch-test-support`

Handy-dandy Elasticsearch integration testing utility that starts a local Docker container running Elasticsearch if you're not running in a CI/CD pipeline.
This allows you to run integration tests locally in a manner similar to how they'd be run in the CI/CD pipeline. 
This module does nothing when running in a CI build pipeline, because Elasticsearch should be configured as part of the build via something like [`.gitlab-ci.yml`'s `services`](https://docs.gitlab.com/ee/ci/yaml/#services) element.

This package is intended to be installed in your project in `devDependencies`.

Your application must install its desired version of [`@elastic/elasticsearch`](https://www.npmjs.com/package/@elastic/elasticsearch).

> NOTE: requires a Unix-y shell (`/usr/bin/env sh`) to be available.
>This is not designed to run on Windows; PRs/MRs welcome.

Usage:
```javascript
const elasticsearchConnect = require('@northscaler/elasticsearch-test-support')

const client = await elasticsearchConnect()

// now you can use the elasticsearch client
```

## Configuration

The default configuration is pretty conventional, with the sole exception of the default port that Elasticsearch will listen on for clients.
Instead of `9200`, which might already be in use on developers' machines when they run integration tests, the default configuration uses `19200`.
It is a `TODO` to search for an available port.

>NOTE: This module detects when it's running in a CI/CD pipeline by seeing if the environment variable `CI` is of nonzero length.

### Environment variables

The following environment variables can be set to configure the docker container:
* ELASTICSEARCH_TEST_SUPPORT_TAG: The tag of the [`elasticsearch` Docker image](https://hub.docker.com/r/bitnami/elasticsearch) or custom image to use, default `latest`
* ELASTICSEARCH_TEST_SUPPORT_PORT: visible client port on `localhost` to map to container port, default is content of `elasticsearch/default-elasticsearch-test-port`
* ELASTICSEARCH_TEST_SUPPORT_CONTAINER: name of container, default is content of file `elasticsearch/default-elasticsearch-test-container`
* ELASTICSEARCH_TEST_SUPPORT_CONTAINER_PORT: elasticsearch client port in container, default `9200`
* ELASTICSEARCH_TEST_SUPPORT_IMAGE: docker image name, default `bitnami/elasticsearch`
