'use strict'

const fs = require('fs')
const pause = require('./pause')
const { Client } = require('@elastic/elasticsearch')

const startElasticsearch = require('./start-elasticsearch')

const defaultContainerName = fs.readFileSync(`${__dirname}/default-elasticsearch-test-container`).toString('utf8').trim()
const defaultPort = parseInt(fs.readFileSync(`${__dirname}/default-elasticsearch-test-port`))
const defaultNode = `http://localhost:${defaultPort}`

let client

/**
 * Starts an Elasticsearch container.
 * @param {object} [opts] Elasticsearch client options.
 * @param {object} [arg1] Ping retry options.
 * @param {number} [arg1.maxTries=10] The max number of tries to ping.
 * @param {number} [arg1.retryPauseMillis=500] The number of milliseconds to pause between ping attempts.
 * @return {Promise<Client>}
 */
async function elasticsearchConnect (
  opts, {
    maxTries = 50,
    retryPauseMillis = 1000,
    scriptArgs = [],
    pauseMillis = 0
  } = {}) {
  if (client) return client

  opts = opts || {}
  opts.node = opts.node || defaultNode

  await startElasticsearch({ scriptArgs, pauseMillis })

  let tries = 0
  do {
    try {
      client = new Client(opts)
      await client.ping()
    } catch (e) {
      if (++tries >= maxTries) throw e
      console.log(`error pinging, but will retry: ${e}`)
      await pause(retryPauseMillis)
      client = null
    }
  } while (!client)

  return client
}

/**
 * The default test port that Elasticsearch will listen on for incoming `Client`s.
 * @type {number}
 */
elasticsearchConnect.defaultPort = defaultPort

/**
 * The default Elasticsearch test container name.
 * @type {string}
 */
elasticsearchConnect.defaultContainerName = defaultContainerName

module.exports = elasticsearchConnect
