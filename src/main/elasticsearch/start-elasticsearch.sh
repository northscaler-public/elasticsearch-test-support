#!/usr/bin/env sh

# Usage is via env vars:
#   ELASTICSEARCH_TEST_SUPPORT_TAG: docker image tag to use, default "latest"
#   ELASTICSEARCH_TEST_SUPPORT_PORT: visible port to map to container port, default is content of file default-elasticsearch-test-port
#   ELASTICSEARCH_TEST_SUPPORT_CONTAINER: name of container, default is content of file default-elasticsearch-test-container
#   ELASTICSEARCH_TEST_SUPPORT_CONTAINER_PORT: elasticsearch client port in container, default 9042
#   ELASTICSEARCH_TEST_SUPPORT_IMAGE: docker image name, default "bitnmai/elasticsearch"

THIS_DIR="$(cd "$(dirname "$0")"; pwd)"

if [ -n "$CI" ]; then # we're in CI pipeline & not forcing start
  echo 'in CI pipeline; container is assumed to be started'
  exit 0
fi

ELASTICSEARCH_TEST_SUPPORT_TAG=${ELASTICSEARCH_TEST_SUPPORT_TAG:-latest}
ELASTICSEARCH_TEST_SUPPORT_CONTAINER_PORT=${ELASTICSEARCH_TEST_SUPPORT_CONTAINER_PORT:-9042}
ELASTICSEARCH_TEST_SUPPORT_CONTAINER_IMAGE=${ELASTICSEARCH_TEST_SUPPORT_CONTAINER_IMAGE:-elasticsearch}

if [ -z "$ELASTICSEARCH_TEST_SUPPORT_CONTAINER" ]; then
  ELASTICSEARCH_TEST_SUPPORT_CONTAINER="$(cat $THIS_DIR/default-elasticsearch-test-container)"
fi

if [ -z "$ELASTICSEARCH_TEST_SUPPORT_PORT" ]; then
  ELASTICSEARCH_TEST_SUPPORT_PORT="$(cat $THIS_DIR/default-elasticsearch-test-port)"
fi

if [ -z "$(docker ps --quiet --filter name=$ELASTICSEARCH_TEST_SUPPORT_CONTAINER)" ]; then
  "$THIS_DIR/start-elasticsearch-container.sh"
fi
