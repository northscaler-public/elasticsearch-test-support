#!/usr/bin/env sh

# Usage is via env vars:
#   ELASTICSEARCH_TEST_SUPPORT_TAG: docker image tag to use, default "latest"
#   ELASTICSEARCH_TEST_SUPPORT_PORT: visible port to map to container port, default is content of file default-elasticsearch-test-port
#   ELASTICSEARCH_TEST_SUPPORT_CONTAINER: name of container, default is content of file default-elasticsearch-test-container
#   ELASTICSEARCH_TEST_SUPPORT_CONTAINER_PORT: elasticsearch client port in container, default 9200
#   ELASTICSEARCH_TEST_SUPPORT_IMAGE: docker image name, default "bitnami/elasticsearch"

THIS_DIR="$(cd "$(dirname "$0")"; pwd)"

ELASTICSEARCH_TEST_SUPPORT_TAG=${ELASTICSEARCH_TEST_SUPPORT_TAG:-latest}
ELASTICSEARCH_TEST_SUPPORT_CONTAINER_PORT=${ELASTICSEARCH_TEST_SUPPORT_CONTAINER_PORT:-9200}
ELASTICSEARCH_TEST_SUPPORT_CONTAINER_IMAGE=${ELASTICSEARCH_TEST_SUPPORT_CONTAINER_IMAGE:-bitnami/elasticsearch}

if [ -z "$ELASTICSEARCH_TEST_SUPPORT_CONTAINER" ]; then
  ELASTICSEARCH_TEST_SUPPORT_CONTAINER="$(cat $THIS_DIR/default-elasticsearch-test-container)"
fi

if [ -z "$ELASTICSEARCH_TEST_SUPPORT_PORT" ]; then
  ELASTICSEARCH_TEST_SUPPORT_PORT="$(cat $THIS_DIR/default-elasticsearch-test-port)"
fi

RUNNING=$(docker inspect --format="{{ .State.Running }}" "$ELASTICSEARCH_TEST_SUPPORT_CONTAINER" 2> /dev/null)

if [ "$RUNNING" == "true" ]; then
  exit 0
fi

# else container is stopped or unknown - forcefully recreate
echo "container '$ELASTICSEARCH_TEST_SUPPORT_CONTAINER' is stopped or unknown - recreating"

# make sure it's gone
docker ps -a | \
  grep "$ELASTICSEARCH_TEST_SUPPORT_CONTAINER" | \
  awk '{ print $1}' | \
  xargs docker rm --force

CMD="docker run --name $ELASTICSEARCH_TEST_SUPPORT_CONTAINER  -p $ELASTICSEARCH_TEST_SUPPORT_PORT:$ELASTICSEARCH_TEST_SUPPORT_CONTAINER_PORT -d -e ELASTICSEARCH_PORT_NUMBER=$ELASTICSEARCH_TEST_SUPPORT_CONTAINER_PORT  $ELASTICSEARCH_TEST_SUPPORT_CONTAINER_IMAGE:$ELASTICSEARCH_TEST_SUPPORT_TAG"
echo "$CMD"
$CMD

#docker run \
#  --name "$ELASTICSEARCH_TEST_SUPPORT_CONTAINER" \
#  -p "$ELASTICSEARCH_TEST_SUPPORT_PORT:$ELASTICSEARCH_TEST_SUPPORT_CONTAINER_PORT" \
#  -d \
#  -e "ELASTICSEARCH_PORT_NUMBER=$ELASTICSEARCH_TEST_SUPPORT_CONTAINER_PORT" \
#  "$ELASTICSEARCH_TEST_SUPPORT_CONTAINER_IMAGE:$ELASTICSEARCH_TEST_SUPPORT_TAG"
