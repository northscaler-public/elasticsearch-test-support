/* global describe, it */
'use strict'

const chai = require('chai')
chai.use(require('dirty-chai'))
const expect = chai.expect

const elasticsearchConnect = require('../../../main')

describe('integration tests of elasticsearch', function () {
  describe('elasticsearch-connect', function () {
    it('should work', async function () {
      if (process.env.CI) { // don't run this in CI pipeline
        console.log('skipping because in CI pipeline')
        return
      }

      this.timeout(60000)

      const client = await elasticsearchConnect()

      expect(client).to.be.ok()
    })
  })
})
